class Maze:
    def __init__(self):
        self.maze = []
        self.wall = '#'
        self.empty_cell = ' '
        self.start = 'X'
        self.exit = '0'

    def open(self, file):
        with open(file, 'r') as mazeFile:
            for line in mazeFile:
                self.maze.append(list(line.rstrip('\n')))
        return self

    def find_start(self):
        for i, row in enumerate(self.maze):
            for j, cel in enumerate(row):
                if cel == self.start:
                    return i, j

    def __str__(self):
        return repr(self)

    def __repr__(self):
        result = ''
        for row in self.maze:
            for cel in row:
                if cel == self.wall:
                    result += '\x1b[5;30;46m' + '  ' + '\x1b[0m'
                elif cel == self.start:
                    result += '\x1b[5;31;41m' + 'XX' + '\x1b[0m'
                elif cel == self.empty_cell:
                    result += '  '
                elif cel == '0':
                    result += '\x1b[5;31;42m' + '  ' + '\x1b[0m'
                elif str(cel).isdigit() and int(cel) > 0:
                    result += '\x1b[5;30;47m' + str(cel).rjust(2, ' ') + '\x1b[0m'
                else:
                    result += str(cel).rjust(2, ' ')
            result += '\n'
        return result

    def find(self, searchItem):
        mz = self.maze
        processed = set([])
        step = 1
        points = [self.find_start() + ([],)]

        def fill_cell(x, y, current):
            if ((x, y) not in processed and
                -1 < x < len(mz) and
                -1 < y < len(mz[x]) and
                (mz[x][y] == maze.empty_cell or
                 mz[x][y] == searchItem)):

                processed.add((x, y))
                prev = list(current[2])
                prev.append((current[0], current[1]))

                new_point = (x, y, prev)
                new_points.append(new_point)

        exit_not_found = True
        exit_point = None
        while exit_not_found and len(points) > 0:
            new_points = []
            for point in points:
                i = point[0]
                j = point[1]
                if mz[i][j] == maze.exit:
                    exit_not_found = False
                    exit_point = point
                    break
                fill_cell(i - 1, j, point)
                fill_cell(i + 1, j, point)
                fill_cell(i, j - 1, point)
                fill_cell(i, j + 1, point)

            points.clear()
            points.extend(new_points)
            new_points.clear()

            step += 1

        new_points.clear()
        points.clear()
        path_len = len(point[2])

        for i, hist in enumerate(exit_point[2]):
            if 0 < i < path_len:
                mz[hist[0]][hist[1]] = i

maze = Maze().open('maze.txt')
print(maze)

print('----------------------------------------------------\n')
maze.find('0')
print(maze)